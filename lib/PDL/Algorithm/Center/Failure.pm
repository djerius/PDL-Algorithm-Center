package PDL::Algorithm::Center::Failure;

# ABSTRACT: Exception classes for PDL::Algorithm::Center

use v5.10;
use strict;
use warnings;

our $VERSION = '0.16';

use custom::failures::x::alias -suffix => '_failure', qw[
  parameter
  iteration::limit_reached
  iteration::empty
];

# COPYRIGHT

1;

__END__

=for Pod::Coverage
iteration_empty_failure
iteration_limit_reached_failure
parameter_failure

=cut
